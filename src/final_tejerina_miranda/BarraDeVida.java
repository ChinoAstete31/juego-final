package final_tejerina_miranda;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class BarraDeVida extends Rectangle{
	
	static final int ANCHO_BARRA = 25;
	static final int ALTO_BARRA = 10;
	private double anchoCancha;
    private Personaje personaje;
    
	public BarraDeVida() {
		super(ANCHO_BARRA, ALTO_BARRA, Color.GREEN);
		
	}
}
