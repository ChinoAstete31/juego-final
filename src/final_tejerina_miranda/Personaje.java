package final_tejerina_miranda;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Personaje extends Rectangle {

	static final int ANCHO_PERSONAJE = 40;
	static final int ALTO_PERSONAJE = 100;
	
	private double altoCancha;
	private double anchoCancha;
	
	public Personaje(double anchoCancha, double altoCancha) {
		super(ANCHO_PERSONAJE, ALTO_PERSONAJE, Color.GREEN);
		setX(anchoCancha / 2 - ANCHO_PERSONAJE/2);
		setY(0  - ALTO_PERSONAJE/altoCancha);
	}
	
	
	public void mover(double desplazamientoX) {
		if ((this.getX() < 0 && desplazamientoX < 0 )
				|| (desplazamientoX > 0 && this.getX()+ANCHO_PERSONAJE < anchoCancha)) {
			return;
		}
		setX(this.getX() + desplazamientoX);
	}
	
}

