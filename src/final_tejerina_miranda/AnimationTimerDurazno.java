package final_tejerina_miranda;

import javafx.animation.AnimationTimer;

public class AnimationTimerDurazno extends AnimationTimer {

	private MiDurazno durazno;

	public AnimationTimerDurazno(MiDurazno durazno) {
		this.durazno=durazno;
	}

	@Override
	public void handle(long arg0) 
	{
		durazno.mover();
	}

}
