package final_tejerina_miranda;


import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class MiDurazno extends Circle{
	private final double VELOCIDAD_MAXIMA = 2;
	private double velocidadX;
	private double velocidadY;
	private boolean enMovimiento;
	private double anchoCancha = 10;
	private double altoCancha = 10;
	private Personaje personaje;
	private boolean destruido;
	
	public MiDurazno(double radio, double anchoCancha, double altoCancha) {
		super(radio);
		setFill(Color.ORANGE);
		velocidadX = Math.random() * 2 * VELOCIDAD_MAXIMA - VELOCIDAD_MAXIMA;
		velocidadY = +Math.random() * VELOCIDAD_MAXIMA + 1;
		this.enMovimiento = false;
		this.altoCancha =  altoCancha;
		this.anchoCancha = anchoCancha;
		setCenterX(Math.random()*500);
		setCenterY(altoCancha  - getRadius());
	}
	public void setPersonaje(Personaje personaje) {
		this.personaje = personaje;
	}
	public void mover()
	{
		if (enMovimiento)
			return;
		
		
		double x = super.getCenterX();
		double y = super.getCenterY();
		double radio = super.getRadius();

		x += velocidadX;
		y += velocidadY;

		if (y + radio >= altoCancha) {
			enMovimiento = false;
		}

		if (y - radio <= 0) {
			velocidadY = -velocidadY;
		}
		
		if ((x + radio >= anchoCancha)
				|| ( x-radio <= 0) ) {
			velocidadX = -velocidadX;
		}
		
		revisarReboteContraPersonaje(x,y,radio);

		super.setCenterX(x);
		super.setCenterY(y);
	}
	public void revisarReboteContraPersonaje(double x, double y, double radio) {

		double xPaleta = personaje.getX();
		double yPaleta = personaje.getY();
		
		// borde superior
		if (velocidadY > 0
				&& x >= xPaleta
				&& x <= xPaleta + personaje.ANCHO_PERSONAJE
				&& y >= yPaleta - radio) {
			destruido = true;
		}

		// borde izquierdo
		if (velocidadX > 0
				&& y >= yPaleta
				&& y <= yPaleta + personaje.ALTO_PERSONAJE
				&& x >= xPaleta - radio) {
			destruido = true;
		}

		// borde derecho
		if (velocidadX < 0
				&& y >= yPaleta
				&& y <= yPaleta + personaje.ALTO_PERSONAJE 
				&& x <= xPaleta + personaje.ANCHO_PERSONAJE + radio) {
			destruido = true;}
		}
	public void setAltoCancha(double altoCancha) {
		this.altoCancha = altoCancha;
	}
	
	public void setAnchoCancha(double anchoCancha) {
		this.anchoCancha = anchoCancha;
	}

	public boolean getEstado() {
		return enMovimiento;
	}
	public void activarMovimiento() {
		enMovimiento = true;	
	}
	void destruir() {
		setFill(Color.WHITE);
		destruido = true;
	}
	
	boolean estaDestruido() {
		return destruido;
	}
    	
}
